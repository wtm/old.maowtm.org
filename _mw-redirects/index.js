addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request));
});

const www_redirect_domains = [
  "maowtm.org",
  "paper.sc",
  "mww.moe",
  "maowtm.com"
];

async function handleRequest(request) {
  let url = new URL(request.url);

  if (url.protocol == "http:") {
    return redirectTo("https://" + url.hostname + url.pathname + url.search);
  }

  let method = request.method;
  if (url.hostname == "static.maowtm.org" && method == "GET") {
    if (url.pathname == "/cv.pdf") {
      return redirectTo("https://maowtm.org/resume.pdf");
    } else {
      return redirectTo("https://old.maowtm.org/static.maowtm.org" + url.pathname + url.search);
    }
  }

  if (url.hostname == "img.maowtm.org" && method == "GET") {
    return redirectTo("https://old.maowtm.org/img.maowtm.org" + url.pathname + url.search);
  }

  if (url.hostname == "death.maowtm.org" && method == "GET") {
    if (url.pathname == "/") {
      return redirectTo("https://postmortem.maowtm.org/?from=d");
    } else {
      return notFound(url);
    }
  }

  if (url.hostname == "maowtm.com" && method == "GET") {
    return redirectTo("https://maowtm.org");
  }

  if (method == "GET" && url.hostname.startsWith("www.")) {
    let domain = url.hostname.substring(4);
    if (www_redirect_domains.includes(domain)) {
      return redirectTo("https://" + domain + url.pathname + url.search);
    }
  }

  return await fetch(request);
}

function redirectTo(url) {
  return new Response(null, {
    status: 302,
    headers: {
      'Location': url,
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, HEAD, OPTIONS',
      'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
    }
  });
}

function notFound(url) {
  return new Response("No content available here.", {
    status: 404,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, HEAD, OPTIONS',
      'Content-Type': 'text/plain',
      'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
    }
  });
}
