// https://stackoverflow.com/a/6234804/4013790
const escapeHtml = (html) => {
    return html.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll('"', '&quot;').replaceAll("'", '&#039;');
}

export const onRequest = async ({ request, next }) => {
  let res = await next();
  if (res.status == 404) {
    let path = new URL(request.url).pathname;
    const page_html =
`<!-- Mixed and minified html + css:
     Source:      https://github.com/micromaomao/old-maowtm.org/tree/master/pages/error.pug
     Style sheet: https://github.com/micromaomao/old-maowtm.org/tree/master/style/error.sass -->

<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="icon" size="all" type="image/svg+xml" href="/static.maowtm.org/svg/logo.svg"><link rel="icon" size="1024x1024" href="/img.maowtm.org/s/logo.png"><title>Oops...</title><style>body{color:#212121;background-color:#fff;font-family:"Vollkorn",serif;font-size:16px;line-height:1.4;margin:0;padding:2rem}body p{margin-top:0.6rem;margin-bottom:0.6rem}h1,h2,h3,h4,h5,h6,.title-font{font-family:'Unica One', sans-serif;text-transform:uppercase}h1 code,h1 pre,h2 code,h2 pre,h3 code,h3 pre,h4 code,h4 pre,h5 code,h5 pre,h6 code,h6 pre,.title-font code,.title-font pre{text-transform:none}*{word-break:break-word}a{text-decoration:none;color:inherit}body .content a,body .copyrights a,body .reflistcontain a,body p a{transition:color 250ms;-webkit-transition:color 250ms;-moz-transition:color 250ms;-ms-transition:color 250ms}body .content:hover a,body .copyrights:hover a,body .reflistcontain:hover a,body p:hover a,body.mobile .content a,body.mobile .copyrights a,body.mobile .reflistcontain a,body.mobile p a{color:#E91E63}@media print{.content a,.copyrights a,.reflistcontain a,p a{color:#E91E63}}body .content a:hover,body .copyrights a:hover,body .reflistcontain a:hover,body p a:hover{color:#C2185B;border-bottom:solid 1px #B6B6B6}body .content a.noline:hover,body .copyrights a.noline:hover,body .reflistcontain a.noline:hover,body p a.noline:hover{border-bottom:none}body .content img,body .copyrights img,body .reflistcontain img,body p img{max-width:100%}i{background-color:rgba(0,150,136,0.1)}pre,code{line-height:1.2;white-space:pre-wrap;word-break:break-all}.shake{display:inline-block}.hiddenText{display:inline;font-size:0;width:0;height:0;opacity:0;margin:0;padding:0}input,textarea,button{font-family:"Vollkorn",serif;outline:none}textarea,button{border-left:solid 1px #009688;border-top:none;border-bottom:solid 1px #009688;border-right:none}button{background-color:white;color:#E91E63}button.primarybutton{background-color:#E91E63;color:white}.copyrights{font-size:0.7rem;color:#727272;margin-top:1rem;border-top:solid 1px rgba(114,114,114,0.4);display:inline-block}.copyrights div{margin-bottom:0.2rem}sup.refsup:target,.reflist li:target{background-color:#ff6}sup.refsup{font-family:sans-serif;font-size:0.7rem;display:inline-block}span.refscope{background-color:rgba(233,30,99,0.1)}.reflistcontain{margin:0.5rem 0;padding:0.5rem 1rem}.reflistcontain .reflist li{list-style-type:none}.reflistcontain .reflist a:hover{border-bottom:none}body{text-align:center;margin-top:8rem;margin-left:auto;margin-right:auto;padding-left:1rem;padding-right:1rem;max-width:800px}body.mobile{text-align:left;margin-top:1rem;padding-left:1rem;padding-right:1rem;max-width:none}p a{color:#E91E63}.errmsg{color:#C2185B}.path{color:#009688}.logo{width:5rem}h1{font-size:1.2rem}h1 .desc{color:#727272}.stack{text-align:left;font-size:0.5rem;margin-left:auto;margin-right:auto;display:inline-block} </style></head><body class="mobile"><img class="logo" src="/static.maowtm.org/svg/logo.svg"><h1>404...&nbsp;<span class="desc">That's weird</span></h1><p>We don't know what to do with the requested path&nbsp;<code class="path">${escapeHtml(path)}</code></p><script src="/static.maowtm.org/script/layout.js"></script><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Unica+One|Vollkorn:400,400i,700"></body></html>`;
    return new Response(page_html, {
      status: 404,
      headers: {
        'Content-Type': 'text/html',
      }
    });
  } else {
    return res;
  }
}
